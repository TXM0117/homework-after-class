import router from '@/router'
import store from '@/store'

// 路由守卫
router.beforeEach(async(to, form, next) => {
  if (store.getters.token) {
    // 已登录
    if (to.path === 'login') {
      // 不能去登录页，踢回首页
      next('/')
    } else {
      // 在已登录，请正常放行的情况，需要保证用户数据存在，才可以放行
      // 没有用户数据的话 再次发送请求获取数据
      if (!store.state.user.userInfo.userId) {
        await store.dispatch('user/getUserInfo')
      }
      next()
    }
  } else {
    // 未登录
    // 判断是不是白名单的成员
    const whiteList = ['/login', '/404', '/test']
    if (whiteList.includes(to.path)) {
      // 白名单放行
      next()
    } else {
      next('/login')
    }
  }
})
