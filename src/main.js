import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

import img from '@/assets/common/head.jpg'
// 使用自定义指令 处理图片问题
Vue.directive('fiximg', {
  // 使用这个指令的时刻会被执行
  inserted(el) {
    console.log('dom元素被插入到页面里面', el)
    el.addEventListener('error', () => {
      el.src = img
    })
    // 额外处理空的状态图片
    // 如果el。src为空 img填入   如果el.src有数据就用自生的数据
    el.src = el.src || img
  }
})

// 封装一个自定义指令, 控制输入框的自动聚焦
Vue.directive('focus', {
  inserted(el) {
    // 聚焦元素
    el.focus()
  }
})

// 使用自己封装的插件
import myComponents from '@/components/myComponents'
// Vue.use 是 vuejs 中的插件机制
// 如果你想要创建一个插件, 只需要导出一个函数即可
// 这个函数会在 Vue.use 执行的时候自动调用
// 还有另外一种写法, 是导出一个对象也行, 但是这种写法必须带有 install 函数
// 是这个 install 函数被执行
Vue.use(myComponents)

// 全局注册过滤器
import { formatDate } from '@/filters/index'
Vue.use('formatDate', formatDate)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
