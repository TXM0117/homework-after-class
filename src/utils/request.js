import axios from 'axios'
import { Message } from 'element-ui'// 引入提示弹框
import store from '@/store'
import router from '@/router'

// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// 请求拦截器
service.interceptors.request.use(config => {
  // 注入token
  if (store.getters.token) {
    // 如果token存在  注入token
    config.headers.Authorization = `Bearer ${store.getters.token}`
    // 超时登录业务 - 主动
    if (Date.now() - localStorage.getItem('time') > 1000 * 60 * 60 * 2) {
      // 调用actions 退出登录并清理本地存储vuex存储得数据
      store.dispatch('user/logout')
      // 退回登录页面
      router.push('/login')
      // 阻止执行，强制报错
      return Promise.reject('登录超时(主动)')
    }
  }
  // 返回放行
  return config
})

// 响应拦截器
service.interceptors.response.use(
  response => {
    // 请求成功 2个状态
    if (response.data.success) {
      // 完全成功
      return response.data.data
    } else {
      // 请求有响应，数据失败
      // 弹出错误提示
      Message.error(response.data.message)
      // 阻止当前promise运行 强制报错
      return Promise.reject(response.data.message)
    }
  }, error => {
  // 完全错误
  // 处理登录token超时-被动处理
  // 单独处理特殊错误，登录超时处理被动提醒
  // 根据和后端得约定好的代码10002来判断
    if (error.response && error.response.data.code === 10002) {
      // 调用actions 退出登录并清理本地存储vuex存储得数据
      store.dispatch('user/logout')
      // 退回登录页面
      router.push('/login')
      // 弹出错误提示
      Message.error('登录超时(被动处理)')
    } else {
      // 弹出错误提示
      console.log(error)
      Message.error(typeof error === 'string' ? error : '系统繁忙,请稍后')
    }
    // 阻止当前promise运行
    return Promise.reject(error)
  }
)
// 导出暴露实例
export default service

/* 响应拦截器统一错误处理封装逐字稿
      1.创建一个axios实例，并通过实例来注册响应拦截器。
      2.在请求出现成功的情况下，响应拦截器会进入第一个回调函数 response 中，
      通过'response.data.success'来判断请求是否成功；如果 success 为true ，则表示请求成功，返回数据的 data部分。
      3.如果 success 为false ，表示数据请求有响应，但是数据请求失败。此时我们通常会给出错误提示框，
      4.在数据请求失败的情况下，我们需要阻止当前Promise运行，并且强制报错
      5.在请求出现错误的情况下，响应拦截器会进入第二参数error中 ；
      给出错误提示，之后阻止当前Promise运行，并且强制报错
*/
