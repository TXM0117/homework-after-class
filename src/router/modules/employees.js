import Layout from '@/layout'
export default {
  path: '/employees', // 路径
  component: Layout, // 组件
  children: [
    {
      path: '',
      component: () => import('@/views/employees'),
      meta: {
        title: '员工 ', // meta属性的里面的属性 随意定义 但是这里为什么要用title呢， 因为左侧导航会读取我们的路由里的meta里面的title作为显示菜单名称
        icon: 'people'
      }
    },
    {
      path: 'import',
      component: () => import('@/views/employees/Import'),
      hidden: true
    },
    {
      path: 'detail/:id',
      component: () => import('@/views/employees/Detail'),
      hidden: true
    },
    {
      path: 'print/:id', // 二级默认路由
      component: () => import('@/views/employees/components/Print.vue'), // 按需加载
      hidden: true
    }

  ]
}
