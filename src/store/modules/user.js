// 引入登录api
import { login, getUserInfo, getUSerDetailById } from '@/api/user'

// 存数据
const state = {
  token: localStorage.getItem('token') || '',
  userInfo: {}
}
// 改数据
const mutations = {
  // 设置token
  setToken(state, token) {
    state.token = token
    // 存入本地存储
    localStorage.setItem('token', token)
  },
  // 删除token
  removeToken(state) {
    state.token = ''
    localStorage.removeItem('token')
  },
  // 存用户信息
  setUserInfo(state, UserInfo) {
    state.userInfo = UserInfo
  },
  // 删除用户信息
  removeUserInfo(state) {
    state.userInfo = {}
  }
}
// 异步发起请求
const actions = {
  // 登录
  async login(context, data) {
    // 1.发送请求
    const result = await login(data)
    // 2.判断是否成功
    // if (result.data.success) {
    // 3.请求成功 存储token
    context.commit('setToken', result)
    // }
    // 登录成功，记录当前得时间戳
    localStorage.setItem('time', Date.now())
  },
  // 获取用户信息
  async getUserInfo(context) {
    const result = await getUserInfo()
    // 获取用户的其他信息 头像等
    const detail = await getUSerDetailById(result.userId)
    context.commit('setUserInfo', {
      ...result,
      ...detail
    })
  },
  // 退出登录
  logout(context) {
    // 连续调用两个 mutations
    context.commit('removeToken')
    context.commit('removeUserInfo')
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
