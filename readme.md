# 项目逐字稿

### 后台项目框架搭建过程逐字稿

1.在gitee码云上提取克隆花裤衩的极简的基础框架到本地

2.下载依赖包   `npm i`

3.清除多余的代码

4.进行严格模式的配置

5.axios 封装代码 ` request.js` 文件

6.修改运行端口和网站的名称

### 响应拦截器统一错误处理封装逐字稿

```js
import axios from 'axios'
import { Message } from 'element-ui'// 引入提示弹框

// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// 请求拦截器
service.interceptors.request.use()

// 响应拦截器
service.interceptors.response.use(
  response => {
    // 请求成功 2个状态
    if (response.data.success) {
      // 完全成功
      return response.data.data
    } else {
      // 请求有响应，数据失败
      // 弹出错误提示
      Message.error(response.data.message)
      // 阻止当前promise运行 强制报错
      return Promise.reject(response.data.message)
    }
  }, error => {
    // 完全错误
  // 弹出错误提示
    console.log(error)
    Message.error('系统繁忙,请稍后')
    // 阻止当前promise运行
    return Promise.reject(error)
  }
)
// 导出暴露实例
export default service
```

1. 创建了一个 axios 实例 `service`，并注册了响应拦截器。
2. 响应拦截器中，通过 `response.data.success` 来判断请求是否完全成功。如果 `success` 为 `true`，则表示请求完全成功，返回响应数据的 `data` 部分。
3. 如果 `success` 为 `false`，则表示数据请求有响应，但是数据请求失败。此时，我们通常会弹出错误提示框，提示用户数据请求失败的原因。代码中使用 `Message.error(response.data.message)` 来弹出错误提示框，并将错误信息 `response.data.message` 作为提示信息。
4. 在数据请求失败的情况下，我们需要阻止当前 Promise 运行，并强制报错。代码中使用 `return Promise.reject(response.data.message)` 来阻止当前 Promise 运行，并将错误信息作为报错信息。
5. 在请求出现错误的情况下，响应拦截器会进入第二个回调函数 `error => {}`。在该回调函数中，我们通常需要对错误进行处理，比如弹出错误提示框并返回错误信息。代码中使用 `Message.error('系统繁忙,请稍后')` 来弹出错误提示框，并返回错误信息 `error`。注意，在请求出现错误的情况下，我们也需要阻止当前 Promise 运行，并强制报错，代码中使用 `return Promise.reject(error)` 来实现这一点。
6. 最后，将创建的 axios 实例 `service` 导出，以便在其他地方使用。可以通过导入这个实例并调用其 `get()`、`post()` 等方法来发送请求。在每个请求中，都会自动执行响应拦截器中的逻辑，从而实现了错误信息的统一处理。

### 登录页面的流程

1.首先用户输入手机号和密码，在失去焦点的时候对表单进行规则校验

2.在发送登录请求前我们可以单独封装一个登录接口，然后在vuex中的actions模块进行登录的逻辑业务处理。

3.用户输入信息完毕，点击登录按钮发送登录请求,  调用actions 来发起异步请求

4.输入的内容会发送给后端进行比对验证。如果验证成功的话，就会返回一个token给前端，我们可以把token存入vuex中的state里，然后使用本地存储的方式进行数据持久化。

5登录成功之后给出弹框提示，然后再跳转到首页。



### 描述登录后用户数据处理的流程

首先第一时间要想到：**获取数据渲染页面**

1.使用路由守卫进行判断有没有 `token` 从而进行页面访问权限的拦截

2.在请求拦截器中 ，判断有没有 `token` ，如果有的话注入到请求头上面

3.在 `Vuex`中封装获取用户的信息，在将获取的用户信息存放在 `Vuex` 中

4.路由守卫检测所有页面的用户数据是否存在，存在的话放行；不存在的话再调用`vuex actions `获取数据

5.按流程渲染Vuex中的用户信息到页面中

### 用户登录时间过久token失效处理方法

**token失效的主动和被动处理方式**

#### 主动处理方式

![image-20230403085320882](D:\重要文件集合\黑马培训\gitee部署\课后作业\homework-after-class\Images\image-20230403085320882.png)

**主动处理方式：判断当前时间-登录时存储的时间戳是不是超时了**

1.用户登录成功把当前登录的时间戳存入本地存储中

2.在请求拦截器中，并且在注入token之后判断当前时间-登录时存储的时间戳是不是超时；如果是执行处理逻辑，如果不是则正常继续请求。

3.当时间超时了，调用Vuex中的actions异步请求 退出登录并清理本地存储和Vuex存储的数据，再返回登录页面

4.阻止执行，强制报错  ` return *Promise*.reject('登录超时(主动)')`

####   被动处理方式

![image-20230403090322726](D:\重要文件集合\黑马培训\gitee部署\课后作业\homework-after-class\Images\image-20230403090322726.png)

**被动处理方式：**根据和后端的约定好的代码10002来判断是不是超时了

1.调用接口发送请求携带token给后端，并和后端约定一下，如果后端发现这个token是错误或者超时了，则会返回一个错误状态码

2.在响应拦截器中，根据和后端的约定好的状态码10002来判断是不是超时了或者token错误

3.当时间超时了或者token错误，调用Vuex中的actions异步请求 退出登录并清理本地存储和Vuex存储的数据，再返回登录页面

4.弹出错误提示。` Message.error('登录超时(被动处理)')`

后端告诉我们超时了

被动处理的方式安全性更高

### vue插件机制 Vue.use 执行逻辑的逐字稿
